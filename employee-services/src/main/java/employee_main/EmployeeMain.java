package employee_main;

import employee_management.*;

public class EmployeeMain {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		City city = new City();
		city.setCityName();city.setCityState();
		Address add = new Address();
		add.setAddLine1();add.setAddLine2();add.setAddZip();add.setCity(city);
		Country count = new Country ();
		count.setCountryName();
		Person p1 = new Person();
		p1.setPersonAddress(add);p1.setPersonContact();p1.setPersonCountry(count);p1.setPersonEmail();p1.setPersonName();
		Employee emp = new Employee();
		emp.printEmployee();emp.setEmpBonus();emp.setEmpDepartment();emp.setEmpDesignation();emp.setEmpsalary();
		System.out.println("\n\n");
		Person p2 = new Person(3, 7417983089L, "Adarsh Srivastava", "ravi271190@gmail.com", add, count);
//		Employee emp1 = new Employee(p2, "Analyst", "IMS", 20000.0, 5000.0);
		Manager m1 = new Manager (p2, 50000.0, 5000.0, "General Electric", "Good");
		m1.printManager();
	}
}
