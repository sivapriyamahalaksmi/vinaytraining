package employee_management;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Person {
	private double personId; 
	private long personContact;
	private String personName, personEmail;
	private Address personAddress;
	private Country personCountry;
	public Person() {
		// Default constructor
		personId = 1;
		personContact = 0;
		personName = "Employee Management Service";
		personEmail = "ems@io.pivotal.workshop";
	}
		public double getPersonId() {
		personId = Math.random();
		return personId;
	}
	public void setPersonId(int personId) {
		this.personId = personId;
	}
	public long getPersonContact() {
		try {
		Scanner input = new Scanner(System.in);
		System.out.println("Contact Number: ");
		personContact = input.nextLong();
		input.close();
		}catch(NumberFormatException nfe) {
			System.err.println("Error: "+nfe);
		}
		return personContact;
	}
	public void setPersonContact() {
		personContact = getPersonContact();
	}
	public String getPersonName() {
		try {
		Scanner input = new Scanner(System.in);
		System.out.println("Employee Name: ");
		personName = input.nextLine();
		input.close();
		}catch(InputMismatchException ime) {
			System.err.println("Error : "+ime);
		}
		return personName;
	}
	public void setPersonName() {
		personName = getPersonName();
	}
	public String getPersonEmail() {
		try {
		Scanner input = new Scanner(System.in);
		System.out.println("Email ID: ");
		personEmail = input.nextLine();
		input.close();
		}catch(InputMismatchException ime) {
			System.err.println("Error :"+ime);
		}
		return personEmail;
	}
	public void setPersonEmail() {
		personEmail = getPersonEmail();
	}
	public Address getPersonAddress() {
		return personAddress;
	}
	public void setPersonAddress(Address personAddress) {
		this.personAddress = personAddress;
	}
	public Country getPersonCountry() {
		return personCountry;
	}
	public void setPersonCountry(Country personCountry) {
		this.personCountry = personCountry;
	}
/*	public String getPersonCity() {
		return personCity;
	}
	public void setPersonCity(String personCity) {
		this.personCity = personCity;
	}
	public String getPersonState() {
		return personState;
	}
	public void setPersonState(String personState) {
		this.personState = personState;
	}
	public String getPersonCountry() {
		return personCountry;
	}
	public void setPersonCountry(String personCountry) {
		this.personCountry = personCountry;
	}
*/	public Person(int personId, long personContact, String personName, String personEmail, Address personAddress,
			Country personCountry) {
		super();
		this.personId = personId;
		this.personContact = personContact;
		this.personName = personName;
		this.personEmail = personEmail;
		this.personAddress = personAddress;
//		this.personCity = personCity;
//		this.personState = personState;
		this.personCountry = personCountry;
	}
	public void printPerson(){
		System.out.println("Person.java class executed");
//		System.out.println("Person ID: "+personId);
		System.out.println("Person Name: "+personName);
		System.out.println("Contact: "+personContact);
		System.out.println("Email ID: "+personEmail);
		personAddress.printAddress();
/*		System.out.println("Address: "+personAddress);
		System.out.println("City: "+personCity);
		System.out.println("State: "+personState);
		System.out.println("country: "+personCountry);
*/		personCountry.printCountry();
	}
}
