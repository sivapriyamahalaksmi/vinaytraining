package employee_management;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Country {
	private int countryId;
	private String countryName;
	public Country() {
		// TODO Auto-generated constructor stub
		countryId = 0;
		countryName = "Earth";
	}
	public int getCountryId() {
		return countryId;
	}
	public void setCountryId(int countryId) {
		this.countryId = countryId;
	}
	public String getCountryName() {
		try {
		Scanner input = new Scanner(System.in);
		System.out.println("Enter you Country");
		countryName = input.nextLine();
		input.close();
		}catch(InputMismatchException ime) {
			System.err.println("Error : "+ime);
		}
		return countryName;
	}
	public void setCountryName() {
		countryName = getCountryName();
	}
	public Country(int countryId, String countryName) {
		super();
		this.countryId = countryId;
		this.countryName = countryName;
	}
	public void printCountry(){
		System.out.println("Country Class executed");
		System.out.println("Country : "+countryName);
	}
}
