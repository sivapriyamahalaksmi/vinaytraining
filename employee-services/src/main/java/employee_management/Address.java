package employee_management;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Address {
	double addId;
	private int addZip;
	private String addLine1, addLine2;
	private City city;
	public Address() {
		// Initialization of the variables
		addId = 0; addZip = 0;
		addLine1 = "EMS Home"; addLine2 = "Local";
	}
	public double getAddId() {
		addId = Math.random();
		return addId;
	}
	public void setAddId(int addId) {
		this.addId = addId;
	}
	public int getAddZip() {
		try {
		Scanner input = new Scanner(System.in);
		System.out.println("Zip Code (Area Code): ");
		addZip = input.nextInt();
		input.close();
		}catch(NumberFormatException nfe) {
			System.err.println("Error : "+nfe);
		}
		return addZip;
	}
	public void setAddZip() {
		addZip = getAddZip();
	}
	public String getAddLine1() {
		try {
		Scanner input = new Scanner(System.in);
		System.out.println("Address Line 1: ");
		addLine1 = input.nextLine();
		input.close();
		}catch(InputMismatchException ime) {
			System.err.println("Error : "+ime);
		}
		return addLine1;
	}
	public void setAddLine1() {
		addLine1 = getAddLine1();
	}
	public String getAddLine2() {
		try {
		Scanner input = new Scanner(System.in);
		System.out.println("Address Line 1: ");
		addLine2 = input.nextLine();
		input.close();
		}catch(InputMismatchException ime) {
			System.err.println("Error : "+ime);
		}
		return addLine2;
	}
	public void setAddLine2() {
		addLine2 = getAddLine2();
	}
	public City getcity() {
		return city;
	}
	public void setCity(City city) {
		this.city = city;
	}
	public Address(int addZip, String addLine1, String addLine2, City city) {
		super();
//		this.addId = addId;
		this.addZip = addZip;
		this.addLine1 = addLine1;
		this.addLine2 = addLine2;
		this.city = city;
	}
	public void printAddress(){
		System.out.println("Address Class executed");
//		System.out.println("Address ID: "+addId);
		System.out.println("Line 1: "+addLine1);
		System.out.println("Line 2 : "+addLine2);
		System.out.println("Zip Code: "+addZip);
		city.printCity();
	}
}
