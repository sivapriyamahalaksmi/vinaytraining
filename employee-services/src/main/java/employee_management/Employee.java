package employee_management;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Employee{
	private String empDesignation, empDepartment;
	private double empSalary, empBonus;
	private Person emp;
	public Employee() {
		// TODO Auto-generated constructor stub
		empDesignation = "Fresher";
		empDepartment = "Dell Technologies";
		empSalary = 100.0;
		empBonus =10.0;
	}
	public String getEmpDesignation() {
		try {
		Scanner input = new Scanner(System.in);
		System.out.println("Enter the Designation: ");
		empDesignation = input.nextLine();
		input.close();
		}catch(InputMismatchException ime) {
			System.err.println("Error : "+ime);
		}
		return empDesignation;
	}
	public void setEmpDesignation() {
		empDesignation = getEmpDesignation();
	}
	public String getEmpDepartment() {
		try {
		Scanner input = new Scanner(System.in);
		System.out.println("Enter the Designation: ");
		empDepartment = input.nextLine();
		input.close();
		}catch(InputMismatchException ime) {
			System.err.println("Error : "+ime);
		}
		return empDepartment;
	}
	public Person getEmp() {
		return emp;
	}
	public void setEmp(Person emp) {
		this.emp = emp;
	}
	public void setEmpDepartment() {
		empDepartment = getEmpDepartment();
	}
	public double getEmpsalary() {
		try {
		Scanner input = new Scanner(System.in);
		System.out.println("Salary of Employee: ");
		empSalary = input.nextDouble();
		input.close();
		}catch(NumberFormatException nfe) {
			System.err.println("Error : "+nfe);
		}
		return empSalary;
	}
	public void setEmpsalary() {
		empSalary = getEmpsalary();
	}
	public double getEmpBonus() {
		try {
		Scanner input = new Scanner(System.in);
		System.out.println("Bonus of Employee: ");
		empBonus = input.nextDouble();
		input.close();
		}catch(NumberFormatException nfe) {
			System.err.println("Error : "+nfe);
		}
		return empBonus;
	}
	public void setEmpBonus() {
		empBonus = getEmpBonus();
	}
	public Employee(Person emp, String empDesignation, String empDepartment, double empSalary, double empBonus) {
		super();
		this.empDesignation = empDesignation;
		this.empDepartment = empDepartment;
		this.empSalary = empSalary;
		this.empBonus = empBonus;
		this.setEmp(emp);
	}
	public void printEmployee(){
		System.out.println("Employee Class executed");
//		emp.printPerson();
		System.out.println("Designation: "+empDesignation);
		System.out.println("Department: "+empDepartment);
		System.out.println("Salary: "+empSalary);
		System.out.println("Bonus: "+empBonus);
	}
}
