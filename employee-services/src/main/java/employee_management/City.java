package employee_management;

import java.util.InputMismatchException;
import java.util.Scanner;

public class City {
	private int cityId;
	private String cityName, cityState;
	public City() {
		// Initialization of variables
		cityId = 0;
		cityName = "Bangalore"; cityState = "Karnataka";
	}
	public int getCityId() {
		return cityId;
	}
	public void setCityId(int cityId) {
		this.cityId = cityId;
	}
	public String getCityName() {
		try {
		Scanner input = new Scanner(System.in);
		System.out.println("Enter you City");
		cityName = input.nextLine();
		input.close();
		}catch(NumberFormatException ime) {
			System.err.println("Error :"+ime);
		}
		return cityName;
	}
	public void setCityName() {
		cityName = getCityName();
	}
	public String getCityState() {
		try {
		Scanner input = new Scanner(System.in);
		System.out.println("Enter you State");
		cityState = input.nextLine();
		input.close();
		}catch(InputMismatchException ime) {
			System.err.println("Error : "+ime);
		}
		return cityState;
	}
	public void setCityState() {
		cityState = getCityState();
	}
	public City(int cityId, String cityName, String cityState) {
		super();
		this.cityId = cityId;
		this.cityName = cityName;
		this.cityState = cityState;
	}
	public void printCity() {
		System.out.println("City Class executed");
//		System.out.println("City ID: "+cityId);
		System.out.println("City Name: "+cityName);
		System.out.println("City State: "+cityState);
	}
}
