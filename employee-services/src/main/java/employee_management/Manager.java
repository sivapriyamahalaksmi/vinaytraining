package employee_management;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Manager{
	private double mgrSalary, mgrBonus;
	private String mgrProject, mgrRating;
	Person emp;
	public Manager() {
		// TODO Auto-generated constructor stub
		mgrSalary = 1110.0;mgrBonus = 6000.0;
		mgrProject = "Dell"; mgrRating = "Average";
	}
	public double getMgrSalary() {
		try {
		Scanner input = new Scanner(System.in);
		System.out.println("Salary of Manager: ");
		mgrSalary = input.nextDouble();
		input.close();
		}catch(NumberFormatException nfe) {
			System.err.println("Error :"+nfe);
		}
		return mgrSalary;
	}
	public void setMgrSalary() {
		mgrSalary = getMgrSalary();
	}
	public double getMgrBonus() {
		try {
		Scanner input = new Scanner(System.in);
		System.out.println("Salary of Manager: ");
		mgrBonus = input.nextDouble();
		input.close();
		}catch(NumberFormatException nfe) {
			System.err.println("Error : "+nfe);
		}
		return mgrBonus;
	}
	public void setMgrBonus() {
		mgrBonus = getMgrBonus();
	}
	public String getMgrProject() {
		try {
		Scanner input = new Scanner(System.in);
		System.out.println("Enter the Project of Manager: ");
		mgrProject = input.nextLine();
		input.close();
		}catch(InputMismatchException ime) {
			System.err.println("Error : "+ime);
		}
		return mgrProject;
	}
	public void setMgrProject() {
		mgrProject = getMgrProject();
	}
	public String getMgrRating() {
		try {
		Scanner input = new Scanner(System.in);
		System.out.println("Rating (Bad, Average, Good, Best): ");
		mgrRating = input.nextLine();
		input.close();
		}catch(InputMismatchException ime) {
			System.err.println("Error : "+ime);
		}
		return mgrRating;
	}
	public void setMgrRating() {
		mgrRating = getMgrRating();
	}
	public Manager(Person emp, double mgrSalary, double mgrBonus, String mgrProject, String mgrRating) {
		super();
		this.mgrSalary = mgrSalary;
		this.mgrBonus = mgrBonus;
		this.mgrProject = mgrProject;
		this.mgrRating = mgrRating;
		this.emp = emp;
	}
	public void printManager(){
		System.out.println("Manager Class executed");
		emp.printPerson();
		System.out.println("Salary :"+mgrSalary);
		System.out.println("Bonus: "+mgrBonus);
		System.out.println("Project: "+mgrProject);
		System.out.println("Rating: "+mgrRating);
	}
}

