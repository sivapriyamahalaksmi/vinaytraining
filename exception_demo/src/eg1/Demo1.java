package eg1;

public class Demo1 {

	public static void main(String[] args) {

		int a = 10;
		int b = 2;
		int res = 0;

		try {
			res = a / b;
			System.out.println("Result is " + res);
			String s="abc";  // String s=null;--> null object which doesn't have memory & this is null pointer exception example
			System.out.println("Length is "+s.length());
			System.out.println("Args[0]="+args[0]);
			// System.out.println("Resource (file/network adapter /DB connection)|closed");
		} 
//		catch(Exception e) {}  ---> this is the reason parent cannot be first 
		
		catch (ArithmeticException e) {

			System.out.println("you cannot divide by zero" + e.getMessage());
		} catch (NullPointerException e){
			
			System.out.println("Something is Null");
			
		}
		
		catch (Exception e) {
			
			System.out.println("Something is fishy"+e);
		}
		
		finally {

			System.out.println("Resource (file/network adapter /DB connection)|closed");
		}

		// System.out.println("Resource (file/network adapter /DB connection)|closed");
		System.out.println("Hey i wished to be printed too");

	}

	/* public static class h{} --> static can be allowed */

	/* public class m {} ----> 2 public classes are not allowed */

	/* class a{} ---> This is allowed */

	/*
	 * private class f{
	 * 
	 * public static a{} ---> static cannot be defined under an outer class without
	 * static }
	 */
}
