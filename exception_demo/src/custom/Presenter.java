package custom;

public class Presenter {

	public static void main(String[] args) {
		
		
		Validation v=new Validation();
		
	try {
		v.isValidAge(31);
		System.out.println("Age Validated");
	} 
	
	catch (InValidAgeException e) {
		System.out.println(e.getMessage());
	}	
	
	try {   /// Here we have written seperate try block since we are going to check both the exception if we write in one try handler code doesn't execute other part
	v.isValidAssociateId(110112);
		System.out.println("Id Validated");
	}catch (MyException e){
		System.out.println(e.getMessage());
		
	}

}
}