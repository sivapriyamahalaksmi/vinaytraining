package custom;

public class Validation {
	
	public boolean isValidAge(int age) throws InValidAgeException {
		
		if(age<=0) {
			throw new InValidAgeException("Age cannot be 0 or -ve"); // when we are using throw means creating new object so new keyword is used. 
		}
		
		if (age<18) {
			
			throw new InValidAgeException("Common Grow up");
		}
		
		if (age>30) {
			
			throw new InValidAgeException("age should be between 18 -30");
		}
		return true;
		
	}
	
	public boolean isValidAssociateId(long id) {
		
		if (id<1000 || id>20000)
			throw new MyException(" Invalid Association"); /// depends on the class you are extending here we have extended runtime exception
				
		return true;
	}

}
