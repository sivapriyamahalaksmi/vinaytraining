package eg2;

import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class Business {

	public void openFile(String fileName) throws FileNotFoundException {
//      FileOutputStream fis=new FileOutputStream(fileName); ---> should open the file for us 
		FileInputStream fis=new FileInputStream(fileName);
	}

}
