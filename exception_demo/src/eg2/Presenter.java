package eg2;

import java.io.FileNotFoundException;

public class Presenter {

	public static void main(String[] args) {
		
		String fileName="hello.txt";
		Business b=new Business();
		try {
			b.openFile(fileName);
			System.out.println("File opened");
		} catch (FileNotFoundException e) {
			System.out.println(e);
		}

	}

}
