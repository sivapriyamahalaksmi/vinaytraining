package example;

public class Demo {

	public static void main(String[] args) {
		
		Integer i1=10;
		Integer i2=10;
		Integer i3=new Integer(10);
		System.out.println(i1==i2);
		System.out.println(i1==i3);/// Here it compares address since i3 is of reference.
		
		System.out.println(i1.equals(i2));
		System.out.println(i1.equals(i3));
		
		int x=100;
		Integer i4=x;
		System.out.println(i4);
		
		
		Integer i5=600;
		
		int y=i5;
		
		System.out.println(y);
		
		System.out.println(Integer.MAX_VALUE);
		System.out.println(Integer.MIN_VALUE);
		
		String s="123";
		
		int z=Integer.parseInt(s);// you can use parse
		
		float f1=Float.parseFloat(s);
		
		System.out.println(z);
		
		System.out.println(f1);
		
		String a=z+"";
		System.out.println(a);
		
		
		Float f=12.3455f;
		a=f.toString();//any object(not only wrapper) in Java can be converted to string using this tostring();
		
		System.out.println(a);
		
		
		int m=99;
		System.out.println(Integer.toBinaryString(m));// to convert integer to Binary value
		
		
		System.out.println(Integer.toOctalString(m));// to convert integer to Octal value
		
		System.out.println(Integer.toHexString(m));
		
		
		Integer i6=500;
		Integer i7=100;
		
		System.out.println(i6.compareTo(i7));  // will return +ve if the first  value is higher
		
		Integer i8=500;
		System.out.println(i6.compareTo(i8)); // will return zero if the values are same.
		
		Integer i9=1000;
		
		System.out.println(i6.compareTo(i9));  // will return -ve if the first  value is lower.
		
		
		Float f3=i9.floatValue();
		System.out.println(f3);
		
		Long l1=i9.longValue();
		System.out.println(l1);
		
		Double d2=i9.doubleValue();
		System.out.println(d2);
		
		f3=999.988f;
		i5=f3.intValue();
		
		System.out.println(i5);///converted into Integer
		
		f3=999.2888f;
		
		
		System.out.println("Using MAth Value");
		System.out.println(Math.round(f3));
		System.out.println(Math.abs(f3));
		
		
		
	}

	}
