package associate_mapping;

public class employee {
	
	private int e_id;
	private String e_name;
	private int salary;
	private String address;
	private String designation;
	private project p1;
	
	public project getP1() {
		return p1;
	}

	public void setP1(project p1) {
		this.p1 = p1;
	}

	public employee() {}
	
	public int getE_id() {
		return e_id;
	}
	public void setE_id(int e_id) {
		this.e_id = e_id;
	}
	public String getE_name() {
		return e_name;
	}
	public void setE_name(String e_name) {
		this.e_name = e_name;
	}
	public int getSalary() {
		return salary;
	}
	public void setSalary(int salary) {
		this.salary = salary;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public employee(int e_id, String e_name, int salary, String address, String designation,project p1) {
		
		this.e_id = e_id;
		this.e_name = e_name;
		this.salary = salary;
		this.address = address;
		this.designation = designation;
		this.p1=p1;
	}

	public void printEmployee() {
		
		System.out.println("Employee id:"+e_id);
		System.out.println("\nEmployee Name:"+e_name);
		System.out.println("\nEmployee designation:"+designation);
		System.out.println("\nEmployee Salary:"+salary);
		System.out.println("\nEmployee Address:"+address);
		p1.printProject();
	}
	
}
