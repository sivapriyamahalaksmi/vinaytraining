package associate_mapping;

public class project {
	
	private int pid;
	private String p_name;
	private int duration;
	
	public project() {}
	
	public int getPid() {
		return pid;
	}
	public void setPid(int pid) {
		this.pid = pid;
	}
	public String getP_name() {
		return p_name;
	}
	public void setP_name(String p_name) {
		this.p_name = p_name;
	}
	public int getDuration() {
		return duration;
	}
	public void setDuration(int duration) {
		this.duration = duration;
	}
	public project(int pid, String p_name, int duration) {
		super();
		this.pid = pid;
		this.p_name = p_name;
		this.duration = duration;
	}
	
	public void printProject () {
		
		System.out.println("\nEmployee project id:"+pid);
		System.out.println("\nEmployee project Name:"+p_name);
		System.out.println("\nEmployee project Duration:"+ duration);
		
		
	}
	
}
