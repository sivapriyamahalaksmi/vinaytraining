package builder_buffer_demo;

public class Demo {

	public static void main(String[] args) {
		
		StringBuilder sb=new StringBuilder("hello");
		
		
		System.out.println(sb);
		sb.append("heyy").append(123).append(true).append(12.788).append('C');
		
		System.out.println(sb);
		
		sb.insert(2,"Java");
		
		System.out.println(sb);
		
		sb.delete(5, 8); ///delete will happen only from index 5-7 not 8
		
		System.out.println(sb);
		
		
		sb.deleteCharAt(1);
		
		System.out.println(sb);
		System.out.println(sb.reverse());
		
	}
	

}
