package builder_buffer_demo;

public class Titlecase {

	public static void main(String[] args) {
		
		String s="hello hi Good afternoon feeling sleepy";
		
		String a[]=s.split(" ");//specifying delimeter through which it should break
		
		StringBuilder sb=new StringBuilder();

		for (int i = 0; i < a.length; i++) {
			sb.append(Character.toUpperCase(a[i].charAt(0)));
			sb.append(a[i].substring(1));// substring will display from 1st index till the end
			sb.append(" ");
						
		}
		
		System.out.println(sb.toString().trim());

	}

}
