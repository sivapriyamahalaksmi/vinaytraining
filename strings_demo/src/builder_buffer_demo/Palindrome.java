package builder_buffer_demo;

public class Palindrome {

	public static void main(String[] args) {
		
		int x=1211;
		String s=x+""; /// Append +"" will convert integer to String
		StringBuilder sb=new StringBuilder(s);
		sb.reverse();
		if(sb.toString().equals(s)) //converting it to string is because sb is string buffer object and s is string object. so we need to convert string buffer into string object
			
		{
			System.out.println("Palin");
		}
		
		else
		{
			System.out.println("Not Palin");
			}
		
		String s1="madam";
		
		if(new StringBuffer(s1).reverse().toString().equals(s1)) {   ///need to convert it it string object
		
			System.out.println("Palin in short way");

	}
		else
		{
			System.out.println("Not Palin in short way");
			}

}
}
