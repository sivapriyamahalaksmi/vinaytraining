package eg;

public class CharApiDemo {

	public static void main(String[] args) {
		String s="HellOO 123 @ qss ^ ..|| 4";
		
		System.out.println("Total length of String"+s.length());

		int alpha=0 , upper=0,lower=0, digit=0,aplhanum=0,whitespace=0,special=0;
		
		for (int i = 0; i < s.length(); i++) {
			
			char c=s.charAt(i);
			
			if(Character.isAlphabetic(c)) 
			{
				alpha++;
			}
			
			if(Character.isUpperCase(c)) {
				upper++;
			}
			
			if(Character.isLowerCase(c)){
				lower++;
			}
			
			if(Character.isDigit(c)) 
			{
				digit++;
			}
			
			if(Character.isLetterOrDigit(c)) {
				
				aplhanum++;
			}
			
			
			if(Character.isWhitespace(c)) {
				
				whitespace++;
			}
			
			
			if(!Character.isLetterOrDigit(c)&&!Character.isWhitespace(c)) {
				
				special++;
				
			}
		}
		
		
		System.out.println("Alpha Count:"+alpha);
		System.out.println("Upper case:"+upper);
		System.out.println("Lower case:"+lower);
		System.out.println("Digits:"+digit);
		System.out.println("Alpha numeric:"+aplhanum);
		System.out.println("Whitespace:"+whitespace);
		System.out.println("special characters:"+special);
		
	}

}
