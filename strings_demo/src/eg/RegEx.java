package eg;

public class RegEx {

	public static void main(String[] args) {
		
		/*
		 * Regular Expression(RegEx)-PMA - applicable to all Modular programming language with minor syntactic changes
		 * 
		 * Reduces so much lines of code by testing using single expression or single line
		 * 
		 * []- represent expression
		 * 
		 * {}- represent Length
		 * 
		 * [a-z]{2}- any 2 lower case letters
		 * [A-Z]{6}- any 6 supper case letters
		 * [a-zA-z]{5}-any alphabet of 5 might be upper case or lower case
		 * 
		 * [0-9]{4,6} - any digit of length minimum of 4 and max to 6
		 * 
		 * [0-9]{1,} -Min 1 digit and max no limit
		 * 
		 * ^ - represent not
		 * [^0-9] -- apart form digits
		 * 
		 * */

		
		String pan="EENSP4093N";
		
		if (pan.matches("[A-Z]{5}[0-9]{4}[A-Z]{1}"))
				{
			
			System.out.println("Valid Pan");
		}
		
		else {
			
			System.out.println("Invalid pan");
		}
		
		String s="HellOO 123 @ qss   ^ ..|| 4";
		
		
		
		System.out.println(s.replaceAll("[^a-zA-Z]",""));
		System.out.println(s.replaceAll("[^a-zA-Z]","").length());
		
		System.out.println(s.replaceAll("[ a-zA-Z0-9]",""));
		
		System.out.println(s.replaceAll("[ a-zA-Z0-9]","").length());
		
		System.out.println("Digits");
		
		System.out.println(s.replaceAll("[^0-9]",""));
		
		System.out.println(s.replaceAll("[^ ]",""));
		
		System.out.println(s.replaceAll("[^ ]","").length());
		
		System.out.println(s.replaceAll("[ ]{1,} "," "));

		String License="Ka01-12345678901";
		
		System.out.println(License.replaceAll("[^a-zA-Z0-9]"," "));
		
		String License1="Ka01 12345678901";
		
		System.out.println(License1.replaceAll("[^a-zA-Z0-9]","-"));
		
	}

}
