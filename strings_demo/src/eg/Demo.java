package eg;

import java.util.Arrays;

public class Demo {

	public static void main(String[] args) {
		
		String s1="hello";
		System.out.println(s1);
		System.out.println(s1.toUpperCase());
		System.out.println(s1);

		System.out.println(s1.equals("hello")); //case sensitive
		System.out.println(s1.equals("Hello"));
		System.out.println(s1.equalsIgnoreCase("hELlo")); //case insensitive
		
		System.out.println(s1.startsWith("HE"));
		System.out.println(s1.startsWith("HE"));
		System.out.println(s1.endsWith("lo"));//case sensitive
		System.out.println(s1.endsWith("LO"));
		
		System.out.println(s1.contains("EL"));
		System.out.println(s1.contains("LE"));
		System.out.println(s1.contains("el"));
		
		System.out.println(s1.length());
		
		System.out.println(s1.charAt(2));
		
		for (int i = 0; i < s1.length(); i++) {
			
			System.out.println(s1.charAt(i));
			
		}
		
         char a[]=s1.toCharArray();
		
		System.out.println(Arrays.toString(a));
		
		System.out.println(s1.substring(1));
		
		System.out.println(s1.substring(1, 4));// it will print value from index 1 till 3, it wont print 4 th index
		
		String s2=" sjhdfshdf lkjjl lklk      ";
		
		System.out.println(s2);
		
		System.out.println(s2.trim());
		
		String s3=" hello   hi	good morning too many methods to remember ";
		
		String s4[]=s3.split(" ");
		
		for (int i = 0; i < s4.length; i++) {
			
			System.out.println(s4[i]);
		}
		
		
		System.out.println(Arrays.toString(s4));
		
		System.out.println("hello".compareTo("hi"));
		
		System.out.println("hx".compareTo("hello"));
		
		System.out.println("hello".compareTo("hello"));
		
		System.out.println("hello".compareToIgnoreCase("HELLO"));
		
	}
	
	
}
