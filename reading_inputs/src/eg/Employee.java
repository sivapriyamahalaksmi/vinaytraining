package eg;

public class Employee {
	
	private String name;
	private int age;
	private char gender;
	private double  Salary;
	private long contact;
	private String msg;
	
	public Employee() {
		// TODO Auto-generated constructor stub
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public char getGender() {
		return gender;
	}

	public void setGender(char gender) {
		this.gender = gender;
	}

	public double getSalary() {
		return Salary;
	}

	public void setSalary(double salary) {
		Salary = salary;
	}

	public long getContact() {
		return contact;
	}

	public void setContact(long contact) {
		this.contact = contact;
	}
	

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Employee(String name, int age, char gender, double salary, long contact, String msg) {
		super();
		this.name = name;
		this.age = age;
		this.gender = gender;
		Salary = salary;
		this.contact = contact;
		this.msg=msg;
	}
	
	
	

}
