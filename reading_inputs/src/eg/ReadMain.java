package eg;

import java.util.Scanner;

public class ReadMain {

	public static void main(String[] args) {
		
   Scanner sc=new Scanner(System.in); // Input stream from console needs to be initialized 
   
   System.out.println(" Hi .. Enter your name");
   
   String name= sc.next();
   
   
   System.out.println(" Enter your age");
   
   int age=sc.nextInt();// nextLong()//nextBoolean() nextDouble();
   
   System.out.println(" Enter your gender");
   
   char gender= sc.next().charAt(0);
   
 System.out.println(" Enter your Phone number");
   
   long  contact= sc.nextLong();
   
System.out.println(" Enter your Salary");
   
   double  salary= sc.nextDouble();
   
System.out.println(" Hi .. Enter your Message");
   
   String msg= sc.nextLine();
  
   //System.out.println(" HellO" + name);
   
  // System.out.println(" Age "+age);
   //System.out.println(" Gender "+gender);
   //System.out.println(" Contact Number "+contact);
  // System.out.println(" Welcome Message "+msg);
   
   //System.out.println(" Salary "+salary);
   
   Employee employee =new Employee(name,age,gender,salary,contact,msg);
    printEmployee(employee);
    
    System.out.println(" Enter Employee 2 Details");
    
    Employee e2=new Employee();
    
    System.out.println("ENter name");
    e2.setName(sc.next());
    System.out.println("ENter Age");
    e2.setAge(sc.nextInt());
    System.out.println("ENter Gender");
    e2.setGender(sc.next().charAt(0));
    System.out.println("ENter Salary");
    e2.setSalary(sc.nextDouble());
    System.out.println("ENter Contact");
    e2.setContact(sc.nextLong());
    System.out.println("ENter Message");
    e2.setMsg(sc.nextLine());
    
    printEmployee(e2);
    
	}
	
	public static void printEmployee(Employee e) {
		
		System.out.println(" Hello" +e.getName());
		System.out.println(" Age" +e.getAge());
		System.out.println(" Gender" +e.getGender());
		System.out.println(" Salary" +e.getSalary());
		System.out.println(" Contact" +e.getContact());
		System.out.println(" Message" +e.getMsg());
		
	}

}
