package pojo;

public class Team {

	private int tid;
	private String caoachName;
	private String origin;
	private String teamName;
	public String getTeamName() {
		return teamName;
	}
	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}
	public Team()
	{}
	public int getTid() {
		return tid;
	}
	public void setTid(int tid) {
		this.tid = tid;
	}
	public String getCaoachName() {
		return caoachName;
	}
	public void setCaoachName(String caoachName) {
		this.caoachName = caoachName;
	}
	public String getOrigin() {
		return origin;
	}
	public void setOrigin(String origin) {
		this.origin = origin;
	}
	public Team(int tid, String caoachName, String origin,String teamName) {
		super();
		this.tid = tid;
		this.teamName=teamName;
		this.caoachName = caoachName;
		this.origin = origin;
	}
	
	public void printteam()
	{
		System.out.println("\n\nTeam Details");
		System.out.println("Team id:"+tid);
		System.out.println("Team origin:"+origin);
		System.out.println("Team Name:"+teamName);
		System.out.println("Team caoach name:"+caoachName);
	}
	
	
}
