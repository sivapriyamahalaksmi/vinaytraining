package pojo;

public class Player extends Person {
	
	private Team team;
	private long score;
	
	public Player()
	{}

	
	public Team getTeam() {
		return team;
	}


	public void setTeam(Team team) {
		this.team = team;
	}


	public long getScore() {
		return score;
	}


	public void setScore(long score) {
		this.score = score;
	}


	public Player(int id, String name,Team team, long score) {
		super(id, name);
		this.team = team;
		this.score = score;
	}
	
	public void printPlayer()
	{
		super.printPerson();
		System.out.println("Player SCore:"+score);
		team.printteam();
	}
	}

