package using_interfaces;

public class Main {

	public static void main(String[] args) {
		System.out.println("Calling from My class");
		MyClass m=new MyClass();
		m.hello();
		m.hey();
		m.myMethod();
		System.out.println(m.hashCode());
		m.common();
		m.gaming();
		
		System.out.println("\n\nFrom My interface");
		MyInterface i=new MyClass();
		i.hello();
		i.hey();
		i.common();
		//i.gaming(); cannot be called as there is no relationship between interface and myint2
		///i.myMethod(); he cannot access method defined in class , he can only access his parent mthods
		System.out.println(i.hashCode());
		

		System.out.println("\n\nFrom My int2");
		MyInt2 m2=new MyClass();
		m2.gaming();
		m2.common();
		System.out.println(m2.hashCode());
		
	}

}
