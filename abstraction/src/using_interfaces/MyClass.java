package using_interfaces;

public class MyClass implements MyInterface,MyInt2{

	@Override
	 public void hello() {
			
		System.out.println("Hello from My interface");
		
		
	}

	@Override
	public void hey() {
		
		///x++; doesn't allow you to modify since it is final
		System.out.println("Hey from My interface");
		
	}
	
	public void myMethod() {
		System.out.println("Method of My class");
		
	}

	@Override
	public void common() {
		System.out.println("Common method for both the interfaces");
		
		
	}

	@Override
	public void gaming() {
		System.out.println("Gaming form MyInt2");
		
	}

}
