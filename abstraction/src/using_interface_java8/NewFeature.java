package using_interface_java8;

public interface NewFeature {
	
	public static void iamStatic() {
		
		System.out.println("Static Methods from Java8");
	}

	default void hiDefault() {
		
		System.out.println("Default Methods from Java8");
	}
	
	void hi();
	void hey();
	void hello();
	///void hello() {}  you cannot specify definition  for abstract method
}

//  if you want to call  method iamStatic() --> call using NewFeature.iamStatic();