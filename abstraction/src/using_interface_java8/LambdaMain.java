package using_interface_java8;

public class LambdaMain {

	public static void main(String[] args) {

		HelloFunctional h = () -> {

			System.out.println("Hey Lambda");
		};

		HelloFunctional h1 = () -> {

			System.out.println("Hey Lambda again");
		};

		SayHello s1 = (name) -> {
			System.out.println("Hello   " + name);
		};
		
		Sum s= (x,y,z)-> {
			return x+y+z;
		};

		h.hello();
		h1.hello();
		s1.sayHi("siva");
		System.out.println(s.add(2, 4, 6));
	}

}
