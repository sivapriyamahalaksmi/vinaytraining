package using_interface_java8;

@FunctionalInterface
public interface HelloFunctional {
	
	void hello();
	//void hey();  ->> It doesn't allow to declare too Many abstract methods
	

}
