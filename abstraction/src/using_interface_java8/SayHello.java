package using_interface_java8;
@FunctionalInterface
public interface SayHello {

	void sayHi(String name);
}
