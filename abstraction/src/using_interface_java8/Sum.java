package using_interface_java8;

@FunctionalInterface
public interface Sum {
	
	int add(int a, int b, int c);

}
