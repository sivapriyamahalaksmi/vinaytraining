package using_abstract_classes;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(" Accessing Dell EMS");
		DellEmployer e=  new DellEmployer();
		e.addEmployee();
		e.commonEmployeeBenefit();
		e.deleteEmployee();
		
		System.out.println(" Accessing Simplilearn EMS");
		
	
		SimplilearnEmployer s=new SimplilearnEmployer();
		s.addEmployee();
		s.commonEmployeeBenefit();
		s.updateEmployee();
		s.deleteEmployee();
		
	}

}
