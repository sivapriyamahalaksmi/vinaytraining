package using_abstract_classes;

public abstract  class EMS {
	
	public abstract void addEmployee();

	public abstract void deleteEmployee ();
	
	public void commonEmployeeBenefit() {
		System.out.println(" Any employee should be paid for the amount of work done");
		
	}
	
}
