package using_interfaces_extends;

public interface Child extends Parent {

	void method2();
	void method3();
	
	public interface Inner{
		
		void inner1();
		void inner2();
		
	}

	
}
