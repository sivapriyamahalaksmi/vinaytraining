package using_interfaces_extends;

public class MyClass implements Child,Child.Inner {

	@Override  ///---> this is called annotation and it gives meta information to complier, It is best practice and even it is not there it dont throw any error 
	public void method1() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void method2() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void method3() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void inner1() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void inner2() {
		// TODO Auto-generated method stub
		
	}
	
	//Child c=new MyClass(); --> access all methods except from MyClass , Inner interface
	//Parent p=new MyClass(); --> access only method 1 &2 , cannot access any method of child class
	//Child.Inner i=new MyClass();  ---> access only inner1();, inner 2(); cannot access anything else
	

}
